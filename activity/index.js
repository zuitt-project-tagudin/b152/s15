let user1 = {
	username: "jacksepticeye",
	email: "seanScepticeye@gmail.com",
	age: 31,
	topics: ["Gaming", "Commentary"]
}

function isLegalAge(obj) {
	if (obj.age >= 18) {
		console.log("Welcome to the Club.")
	} else {
		console.log("You cannot enter the club yet.")
	}
}


function addTopic(str) {
	if (str.length >= 5) {
		user1.topics.push(str);
	} else {
		console.log("Enter a topic with at least 5 characters.")
	}
}


let clubMembers = ["jacksepticeye", "pewdiepie"];

function register(str) {
	if (str.length >= 8) {
		clubMembers.push(str);
		console.log(`${str} has been been added to the club`)
	} else {
		console.log("Please enter a usernmae longer or equal to 8 characters")
	}
}

isLegalAge(user1);
addTopic("Life");
addTopic("Topic");
console.log(user1.topics)
register("FilthyFrank");
register("vsauce");
console.log(clubMembers);