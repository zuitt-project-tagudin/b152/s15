console.log("Hello world s15")



let numString1 = "5";
let numString2 = "10";
let num1 = 4;
let num2 = 6;
let num3 = 1.5;
let num4 = .5;

let sum1 = num1 + num2;
console.log(sum1);

let numString3 = numString1 + sum1;
console.log(numString3);


let samplestr = "charles";
console.log(samplestr + 6);

let diff1 = num1 - num3;
console.log(diff1);

let diff2 = numString2 - num2;
console.log(diff2);

console.log(samplestr - num1);

 function subtract(a, b) {
 	return a - b;
 }

 let diff5 = subtract(100, 10);
 console.log(diff5);

let prod1 = num1 * num2;
let prod2 = numString1 * numString2;

console.log(prod1);
console.log(prod2);


function multiply(a, b) {
 	return a * b;
 }

 let prod3 = multiply(100, 10);
 console.log(prod3);

function quotient(a, b) {
 	return a / b;
 }

 let quot1 = quotient(100, 10);
 console.log(quot1);

 console.log(prod3 * 0);
 console.log(quot1 / 0);

 let text = "ChickenDinner";
 text = "Dinner";
console.log(text);

let sampleNum1 = 3;
let sampleNum2 = "4";
sampleNum1 *= sampleNum2;
console.log(sampleNum1);
console.log(sampleNum2);

let mdas = 1 + 2 - 3 * 4 / 5;
console.log(mdas);

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log(pemdas);

let z = 1;
console.log(++z);

console.log(z++);
console.log(++text);

function con (a) {
    console.log(a);
}

con(1 == 1);


let sampConvert = "234232";
sampConvert = Number(sampConvert);
con(sampConvert);


con(1 == true);
con(1 === true);

con(true != "true");

let isAdmin = false;
let isRegistered = true;
let isLegalAge = true;

con(isRegistered && isLegalAge);
con(isAdmin && isRegistered);


let user1 = {
    username: "peterphoenix_1999",
    age: 28,
    level: 15,
    isGuildAdmin: false
}

let user2 = {
    username: "kingBrodie00",
    age: 13,
    level: 50,
    isGuildAdmin: true
}

con(user1.username);

let auth1 = user1.age >= 18 && user1.level >= 25;
con(auth1);

let auth2 = user2.age >= 18 && user2.level >= 25;
con(auth2);

let auth3 = user1.age >= 10 && user1.isGuildAdmin === true;
con(auth3);

let auth4 = user2.age >= 10 && user2.isGuildAdmin === true;
con(auth4);

let auth5 = user1.age >= 18 || user1.level <= 15;
con(auth5);

let auth6 = user2.age >= 18 || user2.level >= 1;
con(auth6);

let auth7 = user1.level >= 10 || user1.isGuildAdmin === false;
con(auth7);

let auth8 = user1.level >= 50 || user1.isGuildAdmin === false;
con(auth8);

con(isRegistered);
con(!isRegistered);



if(user1.age >= 18) {
    con("You are allowed")
} else {
    con("Not allowed")
};

if(user1.level >= 20) {
    con("not a noob")
} else {
    con("noob")
}


if(user1.isGuildAdmin === true) {
    con("welcome back, guild admin")
} else {
    con("you are not authorized to enter")
}


if (user2.level >= 35) {
    con("knight");
} else if (user1.level >= 25) {
    con("swordie")
} else if (user1.level >= 10) {
    con("rookie")
} else {
    con("novice")
}

let usernameInput1 = "nicoleIsAwesome100";
let passwordInput1 = "iamawesomenicole";

let usernameInput2 = "";
let passwordInput2 = null;

function register(username,password) {

    if (username === "" || password === null) {
        con("complete form")
    } else {
        con("thanks for registering")
    }
}

register(usernameInput2, passwordInput2);



function requirementChecker(level, isGuildAdmin) {
    if (level <= 25 && isGuildAdmin === false) {
        con("welcome to guild")
    } else if(level > 25) {
        con("you are too stronk")
    } else if (isGuildAdmin == true) {
        con("guild admin")
    } 
}

con(user1)
requirementChecker(user2.level, user2.isGuildAdmin);

let user3 = {
    username: "richieBillions",
    age: 20,
    level: 20,
    isGuildAdmin: true
}

requirementChecker(user3.level, user3.isGuildAdmin);

function addNum(num1, num2) {
    if(typeof num1 === "number" && typeof num2 === "number") {
        con("run only if both arguments passed are numbers")
    } else {
        con("one or both arguments are not numbers")
    }
}

addNum(5, 10);
addNum("6", 20);

function dayChecker(day) {
/*    day = day.toLowerCase();

    if (day === "sunday") {
        con("Sunday, wear White")
    } else if (day === "monday") {
        con(day + ", wear Blue")
    } else if (day === "tuesday") {
        con(day + ", wear Green")
    } else if (day === "wednesday") {
        con(day + ", wear Purple")
    } else if (day === "thursday") {
        con(day + ", wear Brown")
    } else if (day === "friday") {
        con(day + ", wear Red")
    } else if (day === "saturday") {
        con(day + ", wear Pink")
    }*/ 
    switch(day.toLowerCase()) {
        case "sunday":
            con("Today is " + day + " ; Wear White")
            break;
        case "monday":
            con("Today is " + day + " ; Wear Blue")
            break;
        case "tuesday":
            con("Today is " + day + " ; Wear Green")
            break;
        case "wednesday":
            con("Today is " + day + " ; Wear Purple")
            break;
        case "thursday":
            con("Today is " + day + " ; Wear Brown")
            break;
        case "friday":
            con("Today is " + day + " ; Wear Red")
            break;
        case "saturday":
            con("Today is " + day + " ; Wear Pink")
            break;
        default:
            con("invalid input")
        
    }
}

function dayRand() {
    let x = Math.floor(Math.random() * 6);
    let day = "";

    if (x === 0) {
        day = "Sunday";
        return day;
    } else if (x === 1) {
        day = "Monday";
        return day;
    } else if (x === 2) {
        day = "Tuesday";
        return day;
    } else if (x === 3) {
        day = "Wednesday";
        return day;
    } else if (x === 4) {
        day = "Thursday";
        return day;
    } else if (x === 5) {
        day = "Friday";
        return day;
    } else if (x === 6) {
        day = "Saturday";
        return day;
    }

}

let day = dayRand();
con(day);

dayChecker(day);


function selectPayter() {
    let x = (Math.floor(Math.random() * 6) + 1);
    let payter = "";

    switch (x) {
        case 1:
        return payter = "Eugene";
        break;
        case 2:
        return payter = "Dennis";
        break;
        case 3:
        return payter = "Vincent";
        break;
        case 4:
        return payter = "Jeremiah";
        break;
        case 5:
        return payter = "Jeremiah";
        break;
        case 6:
        return payter = "Jericho";
        break;
        default:

    }
}



let member = selectPayter();
con(member);

switch(member) {

    case "Eugene":
        con("Your power level is 20000");
        break;
    case "Dennis":
        con("Your power level is 15000");
        break;
    case "Vincent":
        con("Your power level is 14500");
        break;
    case "Jeremiah":
        con("Your power level is 10000");
        break;
    case "Alfred":
        con("Your power level is 8000");
        break;
    default:
        con("invalid")
}


function selectCountry() {
    let x = (Math.floor(Math.random() * 5) + 1);
    let item = "";

    switch (x) {
        case 1:
            return item = "Philippines";
            break;
        case 2:
            return item = "USA";
            break;
        case 3:
            return item = "Japan";
            break;
        case 4:
            return item = "Germany";
            break;
        case 5:
            return item = "Korea";
            break;
        default:

    }
}


let country = selectCountry();
con(country);

function capitalChecker(a) {
    switch (a) {
        case "Philippines":
            con("Manila");
            break;
        case "USA":
            con("Washington D.C.");
            break;
        case "Japan":
            con("Tokyo");
            break;
        case "Germany":
            con("Berlin");
            break;
        default:
            con("Input out of range")
        
    }
}

capitalChecker(country);


let superHero = "Batman"

superHero === "Batman" ? con("you are rich") : con("bruce is richer");



